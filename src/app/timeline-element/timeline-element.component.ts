import { Component, Input, OnInit } from '@angular/core';

export interface TimelineElementData {
  employer: string;
  from: string;
  to: string;
  descriptions: string[];
  technologies: string[];
}

@Component({
  selector: 'app-timeline-element',
  templateUrl: './timeline-element.component.html',
  styleUrls: ['./timeline-element.component.less']
})
export class TimelineElementComponent implements OnInit {

  @Input()
  data!: TimelineElementData;

  constructor() { }

  ngOnInit(): void {
  }

}
