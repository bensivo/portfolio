import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TimelineComponent } from './timeline/timeline.component';
import { TimelineElementComponent } from './timeline-element/timeline-element.component';
import { GreetingComponent } from './greeting/greeting.component';
import { ProjectsComponent } from './projects/projects.component';
import { FallingArrowsComponent } from './falling-arrows/falling-arrows.component';
import { SocialComponent } from './social/social.component';
import { InterestsComponent } from './interests/interests.component';
import { EducationComponent } from './education/education.component';

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    TimelineElementComponent,
    GreetingComponent,
    ProjectsComponent,
    FallingArrowsComponent,
    SocialComponent,
    InterestsComponent,
    EducationComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
