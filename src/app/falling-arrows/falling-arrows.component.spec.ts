import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FallingArrowsComponent } from './falling-arrows.component';

describe('FallingArrowsComponent', () => {
  let component: FallingArrowsComponent;
  let fixture: ComponentFixture<FallingArrowsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FallingArrowsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FallingArrowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
