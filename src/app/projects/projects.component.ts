import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.less']
})
export class ProjectsComponent {

  projects = [
    // {
    //   title: 'kgui',
    //   description: 'A desktop and web client for Kafka, designed to bring Kafka to engineers familiar with Postman. Written in golang and Angular.',
    //   image: '/assets/kgui.png',
    //   link: 'https://kgui.app/',
    // },
    {
      title: 'Mapo',
      description: 'Mapo is a browser-based mind-mapping tool, designed to be simple and intuitive. It was built using Angular, Golang, Linode, and Supabase',
      image: '/assets/mapo.png',
      link: 'https://github.com/bensivo/mapo',
    },
    {
      title: 'kcli',
      description: 'A command-line client for kafka with the ability to manage multiple clusters at once. Written in golang for multi-platform compatibility.',
      image: '/assets/kcli.png',
      link: 'https://gitlab.com/bensivo/kcli',
    },
    {
      title: 'Medium',
      description: 'I write tech articles and tutorials on Medium. Come check out my latest articles and follow me!',
      image: '/assets/medium.png',
      link: 'https://medium.com/@bensivo',
    },
    {
      title: 'This site',
      description: 'This site was built using Angular, and hosted in Gitlab pages. I\'m a firm believer in minimal dependencies, so other UI libraries were used',
      image: '/assets/portfolio.png',
      link: 'https://gitlab.com/bensivo/portfolio',
    },
  ]
}
