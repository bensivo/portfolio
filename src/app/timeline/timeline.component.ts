import { Component, Input, OnInit } from '@angular/core';
import { TimelineElementData } from '../timeline-element/timeline-element.component';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.less']
})
export class TimelineComponent {


  timelineElements: TimelineElementData[] = [
    {
      employer: '7-Eleven - R&D',
      from: 'Jul 2023',
      to: 'Present',
      descriptions: [
        'Staff Engineer - Tech Lead',
        'Established the first Platform Engineering team at 7-Eleven R&D. Owned the development roadmap, architected all applications, and lead day-to-day engineering efforts of the team.',
        'Established the first trunk-based development team in the R&D department. With fully automated E2E testing pipelines, achieved deployments to production multiple times daily, and change lead time of less than 48 hours',
        'Architected and delivered an merge-request tracking service, tracking team-level KPIs, and sending push notifications to code authors and reviewers. Used by over 50 developers daily, averaging dozens of merge requests and hundreds of notifications events per day.',
        'Introduced a new project-planning process, streamlining the planning of new initiatives. Used for more than a dozen new projects in the department',
      ],
      technologies: [
        'AWS',
        'React',
        'Nest.js',
        'Golang',
        'Kubernetes',
        'NX',
      ],
    },
    {
      employer: 'Spacee, Inc.',
      from: 'Jan 2021',
      to: 'Jul 2023',
      descriptions: [
        'Software Engineer, then Technical Lead, then Software Engineering Manager',
        'Architected and led dozens of projects for the Deming Robotics product line, enabling real-time monitoring of retail spaces using IoT cameras, Computer Vision, and Data Science',
        'Built and maintained IoT robotic cameras, capturing pictures and video of retail shelves, for CV / AI assisted inventory management',
        'Architected fully-automated CV / ML training pipelines, enabling fully-automated training and deployment of models to gateway devices in customer networks',
        'Built a custom image-annotation platform for bounding-box image annotations, used for all of Spacee\'s model training. Enabled 5x\'s faster annotation of images compared to off-the-shelf solutions',
      ],
      technologies: [
        'NestJS',
        'Angular',
        'Typescript',
        'Python',
        'Kubernetes',
        'IoT',
        'MQTT',
        'Kafka',
        'Azure',
      ],
    },
    {
      employer: '7-Eleven R&D',
      from: 'Feb 2020',
      to: 'Jan 2021',
      descriptions: [
        'Software Engineer (Contract)',
        'Developed several full-stack web-applications to support ML workloads and ML operations in the 711 Cashierless store',
        'Developed a PoC database-sync pipeline, allowing near real-time replication of product data distributed across all 711 stores in the US. Eventually adopted by the 711 platform team, and deployed across all of North America',
      ],
      technologies: [
        'Java Spring',
        'NestJS',
        'Angular',
        'Kubernetes',
        'Kafka',
      ],
    },
    {
      employer: 'Hitachi Consulting',
      from: 'Feb 2019',
      to: 'Feb 2020',
      descriptions: [
        'Consultant',
        'Technical Lead and Full-stack developer for multiple web-applications for a Fortune 10 Automotive Client.',
      ],
      technologies: [
        'Sitecore CMS',
        'React',
        'AngularJS',
        'Java Spring',
        'Apache Tomcat',
        'AWS',
      ],
    },
    {
      employer: 'UNT Network Security Lab',
      from: 'May 2017',
      to: 'December 2018',
      descriptions: [
        'Undergraduate Research Assistant',
        'Developed a set of Android applications which tracked and reported the performance of CPR in real-time',
        'Deployed the application in the Dallas Parkland Hospital, and during training sessions for the Plano fire department',
      ],
      technologies: [
        'Java',
        'Spring',
        'Android',
        'React',
        'Docker',
      ],
    },
    {
      employer: 'Fidelity Investments',
      from: 'May 2018',
      to: 'August 2018',
      descriptions: [
        'Software Engineering Intern',
        'Migrated internal Fidelity UI libraries from jQuery to React and Angular',
        'Refactored and Docker-ized an existing webapp, and migrated it from on-prem servers to the cloud',
      ],
      technologies: [
        'Typescript',
        'React',
        'Angular',
        'jQuery',
        'Docker',
        'Jenkins',
      ],
    },
    {
      employer: 'Nokia Inc.',
      from: 'May 2017',
      to: 'August 2017',
      descriptions: [
        'Software Design Intern',
        'Created a log-file anomaly detection application, using Python and Natural-Language-Processing techniques',
        'Wrote test orchestration scripts, for automating testing of C++ modules used in mobile base stations',
      ],
      technologies: [
        'Python',
        'Bash',
        'GNU Octave',
      ],
    }
  ]
}
